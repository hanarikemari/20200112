<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="dataClass.*" %>
<%@ page import="java.util.*" %>
<%
	String errorMsg = "";
	if(request.getAttribute("error") != null){
		errorMsg = "<p class='error'>" + (String) request.getAttribute("error") + "</p>";
	}
%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Insert title here</title>
	</head>
	<body>
		<%= errorMsg %>
<%
		switch(request.getParameter("name")){
		case "new":
		case "newDo":
			String nextCd = (String) request.getAttribute("nextCd");
%>
			<div class="new">
				<form action="SearchControler" method="post">
					<label><span>商品コード</span><p class="new_tg"><%=nextCd%></p><input type="hidden" id="product_cd" name="product_cd" value="<%=nextCd%>"></label>
					<label><span>商品名</span><input type="text" id="product_name" name="product_name"></label>
					<label><span>単価</span><input type="number" id="unit_price" name="unit_price"></label>
					<label><span>発売日</span><input type="date" id="release_date" name="release_date"></label>
					<label><span>メーカー</span><input type="text" id="maker_name" name="maker_name"></label>
					<input type="hidden" name="name" value="confirm">
					<input type="hidden" name="confirm" value="new">
					<input type="submit" class="button" value="新規登録">
				</form>
				<form action="SearchControler" method="get">
					<input type="submit" class="button" value="キャンセル">
				</form>
			</div>
<%
	break;
		case "alt":
		case "altDo":
		DataProduct sr = (DataProduct) request.getAttribute("result");
%>
		<div class="alt">
			<form action="SearchControler" method="post">
				<label><span>商品コード</span><input type="text" id="product_cd" name="product_cd" value="<%=sr.getProductCd() %>"></label>
				<label><span>商品名</span><input type="text" id="product_name" name="product_name" value="<%=sr.getProductName() %>"></label>
				<label><span>単価</span><input type="number" id="unit_price" name="unit_price" value="<%=sr.getUnitPrice() %>"></label>
				<label><span>発売日</span><input type="date" id="release_date" name="release_date" value="<%=sr.getReleaseDate() %>"></label>
				<label><span>メーカー</span><input type="text" id="maker_name" name="maker_name" value="<%=sr.getMakerName() %>"></label>

				<input type="hidden" id="product_cd_b" name="product_cd_b" value="<%=sr.getProductCd() %>" >

				<input type="hidden" name="name" value="confirm">
				<input type="hidden" name="confirm" value="alt">
				<input type="submit" class="button" value="変更を保存">
			</form>
			<form action="SearchControler" method="post">

				<input type="hidden" id="product_cd" name="product_cd" value="<%=sr.getProductCd() %>" >
				<input type="hidden" id="product_name" name="product_name" value="<%=sr.getProductName() %>" >
				<input type="hidden" id="unit_price" name="unit_price" value="<%=sr.getUnitPrice() %>" >
				<input type="hidden" id="release_date" name="release_date" value="<%=sr.getReleaseDate() %>" >
				<input type="hidden" id="maker_name" name="maker_name" value="<%=sr.getMakerName() %>" >

				<input type="hidden" name="name" value="confirm">
				<input type="hidden" name="confirm" value="remove">
				<input type="submit" class="button" value="削除">
			</form>
			<form action="SearchControler" method="get">
				<input type="submit" class="button" value="キャンセル">
			</form>
		</div>

<%
		break;
		case "confirm":
			String name = request.getParameter("confirm");
%>
		<div class="confirm">
			<form action="SearchControler" method="post">
				<label><span>商品コード</span><%=request.getParameter("product_cd") %></label>
				<label><span>商品名</span><%=request.getParameter("product_name") %></label>
				<label><span>単価</span><%=request.getParameter("unit_price") %></label>
				<label><span>発売日</span><%=request.getParameter("release_date") %></label>
				<label><span>メーカー</span><%=request.getParameter("maker_name")  %></label>
				<input type="hidden" id="product_cd" name="product_cd" value="<%=request.getParameter("product_cd") %>">
				<input type="hidden" id="product_name" name="product_name" value="<%=request.getParameter("product_name") %>">
				<input type="hidden" id="unit_price" name="unit_price" value="<%=request.getParameter("unit_price") %>">
				<input type="hidden" id="release_date" name="release_date" value="<%=request.getParameter("release_date") %>">
				<input type="hidden" id="maker_name" name="maker_name" value="<%=request.getParameter("maker_name")  %>">
				<input type="hidden" name="name" value="<%=name%>Do">
				<input type="submit" class="button" value="確認">

<%
				if(name.equals("alt")){
%>
					<input type="hidden" id="product_cd_b" name="product_cd_b" value="<%=request.getParameter("product_cd_b") %>" >
<%				} %>

			</form>
			<form action="SearchControler" method="get">
				<input type="submit" class="button" value="キャンセル">
			</form>
		</div>

		<%} %>

	</body>
</html>