<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="dataClass.*" %>
<%@ page import="java.util.*" %>
<%
	ArrayList<DataProduct> result = (ArrayList<DataProduct>) request.getAttribute("result");
	DataSearch DS = (DataSearch) request.getAttribute("DataSearch");
	String productCd = "";
	String productName = "";
	String releaseDateBefore = null;
	String releaseDateAfter = null;
	int unitPriceUpper = -1;
	int unitPriceLower = -1;
	String makerName = "";
	if(DS != null){
		productCd = DS.getProductCd();
		productName = DS.getProductName();
		unitPriceUpper = DS.getUnitPriceUpper();
		unitPriceLower = DS.getUnitPriceLower();
		releaseDateBefore = DS.getReleaseDateBefore();
		releaseDateAfter = DS.getReleaseDateAfter();
		makerName = DS.getMakerName();
	}
	String nodata = "<p class='nodata'>検索結果なし</p>";
%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Insert title here</title>
	</head>
	<body>
		<div class="search">
			<form action="SearchControler" method="post">
				<label><span>商品コード</span><input type="search" id="product_cd" name="product_cd" value="<%=productCd %>"></label>
				<label><span>商品名</span><input type="search" id="product_name" name="product_name" value="<%=productName %>"></label>
				<label><span>単価</span><input type="number" id="unit_price_lower" name="unit_price_lower" value="<%if(unitPriceLower >= 0){%><%=unitPriceLower%><%}%>">
					<input type="number" id="unit_price_upper" name="unit_price_upper" value="<%if(unitPriceUpper >= 0){ %><%=unitPriceUpper%><%}%>"></label>
				<label><span>発売日</span><input type="date" id="release_date_before" name="release_date_before" value="<%=releaseDateBefore %>">
					<input type="date" id="release_date_after" name="release_date_after" value="<%=releaseDateAfter %>"></label>
				<label><span>メーカー</span><input type="search" id="maker_name" name="maker_name" value="<%=makerName %>"></label>
				<input type="hidden" name="name" value="search">
				<input type="submit" value="検索">
			</form>
			<form action="SearchControler" method="post">
				<input type="submit" value="新規登録">
				<input type="hidden" name="name" value="new">
			</form>
		</div>

<%
		if(result != null || result.size() > 0){
%>
			<table>
				<tr class="head-result">
					<th>商品コード</th><th>商品名</th><th>単価</th><th>発売日</th><th>メーカー</th><th>編集</th>
				</tr>
<%
			for(int i = 0; i < result.size(); i++){
%>
				<tr class="result">
					<td><%=result.get(i).getProductCd() %></td>
					<td><%=result.get(i).getProductName() %></td>
					<td><%=result.get(i).getUnitPrice() %></td>
					<td><%=result.get(i).getReleaseDate()%></td>
					<td><%=result.get(i).getMakerName() %></td>
					<td><form action="SearchControler" method="post"><input type="submit" value="編集" class="edit">
					<input type="hidden" name="name" value="alt"><input type="hidden" name="product_cd" value="<%=result.get(i).getProductCd() %>"></form></td>
				</tr>
<% 			} %>
<% 		}else{ %>
			<%= nodata %>
<% 		}%>
		</table>

	</body>
</html>