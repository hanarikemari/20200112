
package logics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dataClass.DataProduct;
import dataClass.DataSearch;

public class DatabaseAccessLogic {


	private final String JDBC_URL = "jdbc:postgresql://localhost:5432/ecdb";
	private final String DB_USER = "hanari";
	private final String DB_PASS = "2191121";
	public String stractSqlUpdate(DataProduct updateDP,DataProduct beforeDP) {
		String sql = "";
		boolean secondFlag = false;

		if(!updateDP.getProductCd().equals(beforeDP.getProductCd())) {

			if(secondFlag) {
				sql += " , ";
			}
				sql += "product_cd = '";
				sql += updateDP.getProductCd();
				sql += "'";
				secondFlag = true;
		}
		if(!updateDP.getProductName().equals(beforeDP.getProductName())) {

			if(secondFlag) {
				sql += " , ";
			}
			sql += "product_name = '";
			sql += updateDP.getProductName();
			sql += "'";
			secondFlag = true;
		}
		if(!(updateDP.getUnitPrice() == beforeDP.getUnitPrice())) {

			if(secondFlag) {
				sql += " , ";
			}
			sql += "unit_price = ";
			sql += updateDP.getUnitPrice();
			secondFlag = true;
		}
		if(!updateDP.getReleaseDate().equals(beforeDP.getReleaseDate())) {

			if(secondFlag) {
				sql += " , ";
			}
			sql += "release_date = '";
			sql += updateDP.getReleaseDate();
			sql += "'";
			secondFlag = true;
		}
		if(!updateDP.getMakerName().equals(beforeDP.getMakerName())) {

			if(secondFlag) {
				sql += " , ";
			}
			sql += "maker_name = '";
			sql += updateDP.getMakerName();
			sql += "'";
			secondFlag = true;
		}
		sql += "where product_cd = ";
		sql += last(beforeDP.getProductCd());
		return sql;
	}

	public String stractSqlSearch(DataSearch DS) {
		String sql = "";
		boolean secondFlag = false;

		if(DS.getProductCd() != "") {
			sql += "product_cd = '";
			sql += DS.getProductCd();
			sql += "'";
			return sql;
		}else {
			//商品名
			if(!(DS.getProductName().equals(""))) {
				sql += "product_name = '";
				sql += DS.getProductName();
				sql += "'";
				secondFlag = true;
			}
			if(!(DS.getMakerName().equals(""))) {
				if(secondFlag) {
					sql += " and ";
				}
				sql += "maker_name = '";
				sql += DS.getMakerName();
				sql += "'";
				secondFlag = true;
			}
			//単価下限
			if(DS.getUnitPriceLower() >= 0) {
				if(secondFlag) {
					sql += " and ";
				}
				sql += "unit_price >= ";
				sql += DS.getUnitPriceLower();
				secondFlag = true;
			}
			//単価上限
			if(DS.getUnitPriceUpper() >= 0) {
				if(secondFlag) {
					sql += " and ";
				}
				sql += "unit_price <= ";
				sql += DS.getUnitPriceUpper();
				secondFlag = true;
			}
			//発売日始
			if(!(DS.getReleaseDateBefore().equals(""))) {
				if(secondFlag) {
					sql += " and ";
				}
				sql += "release_date >= '";
				sql += DS.getReleaseDateBefore();
				sql += "'";
				secondFlag = true;
			}
			//発売日締
			if(!(DS.getReleaseDateAfter().equals(""))) {
				if(secondFlag) {
					sql += " and ";
				}
				sql += "release_date <= '";
				sql += DS.getReleaseDateAfter();
				sql += "'";
				secondFlag = true;
			}

		return sql;
		}
	}

	public String stractSqlRegist(DataProduct DP) {
		String sql ="";

		sql += next(DP.getProductCd());
		sql += next(DP.getProductName());
		sql += next(DP.getUnitPrice());
		sql += next(DP.getReleaseDate());
		sql += next(DP.getMakerName());

		return sql;
	}
	private String next(Object o) {
		String s  = "'" + o + "',";
		return s;
	}
	private String last(Object o) {
		String s  = "'" + o + "'";
		return s;
	}

	public ArrayList<DataProduct> search(DataSearch DS){
		ArrayList<DataProduct> SearchResultList = new ArrayList<>();
		String sqlSearch = stractSqlSearch(DS);
		try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){
			String sql = "SELECT product_cd,product_name,unit_price,release_date,maker_name from product where remove_flag = false and " + sqlSearch +" order by product_cd asc;";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			while(rs.next()) {
				DataProduct sResult = new DataProduct();
				String resultProductCd = rs.getString("product_cd");
				String resultProductName = rs.getString("product_name");
				int resultUnitPrice = rs.getInt("unit_price");
				String resultReleaseDate = rs.getDate("release_date").toString();
				String resultMakerName = rs.getString("maker_name");

				sResult.setProductCd(resultProductCd);
				sResult.setProductName(resultProductName);
				sResult.setUnitPrice(resultUnitPrice);
				sResult.setReleaseDate(resultReleaseDate);
				sResult.setMakerName(resultMakerName);
				SearchResultList.add(sResult);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return SearchResultList;
	}

	public boolean regist(DataProduct DP){
		String sqlRegist = stractSqlRegist(DP);
		try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){
			String sql = "insert into product (product_cd, product_name, unit_price, release_date, maker_name,remove_flag) values(" + sqlRegist + "false);";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			int result = pStmt.executeUpdate();
			if(result != 1) {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean update(DataProduct updateDP,DataProduct beforeDP){
		String sqlUpdate = stractSqlUpdate(updateDP,beforeDP);
		try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){
			String sql = "update product set " + sqlUpdate + ";";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			int result = pStmt.executeUpdate();
			if(result != 1) {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean remove(String cd) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){
			String sql = "update product set remove_flag = true where product_cd = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, cd);
			int result = pStmt.executeUpdate();
			if(result != 1) {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	public ArrayList<Integer> getListCd(){
		ArrayList<Integer> listCd = new ArrayList<>();
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sql = "SELECT product_cd from product order by product_cd asc";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			while(rs.next()) {
				listCd.add(Integer.parseInt(rs.getString("product_cd")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return listCd;
	}
}