package logics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import dataClass.DataProduct;
import dataClass.DataSearch;

public class ProductSearchLogic {
	DatabaseAccessLogic DAL = new DatabaseAccessLogic();

	public DataProduct getCdResult(String productCd) {
		ArrayList<DataProduct> result = DAL.search(parseDS(productCd));
		return result.get(0);
	}
	private DataSearch parseDS(String productCd) {
		DataSearch DS = new DataSearch(productCd,"",-1,-1,"","","");
		return DS;
	}

	public ArrayList<DataProduct> getSearchResult(DataSearch DS) {
		return DAL.search(DS);
	}

	//検索用データクラス作成
	public DataSearch setDataSearch(HttpServletRequest request){
		DataSearch DS;
		String productCd = request.getParameter("product_cd");
		String productName;
		if(request.getParameter("product_name").equals("")){
			productName = "";
		}else {
			productName = request.getParameter("product_name");
		}
		int unitPriceUpper;
		int unitPriceLower;
		try {
			unitPriceUpper =Integer.parseInt(request.getParameter("unit_price_upper"));
		} catch(NumberFormatException e) {
			unitPriceUpper = -1;
		}
		try {
			unitPriceLower =Integer.parseInt(request.getParameter("unit_price_lower"));
		} catch(NumberFormatException e) {
			unitPriceLower = -1;
		}
		//下限と上限が逆の場合入れ替える処理(両方に値が入っている場合のみ)
		if((unitPriceLower >= 0 && unitPriceUpper >= 0) && unitPriceLower > unitPriceUpper) {
			int exchange = unitPriceLower;
			unitPriceLower = unitPriceUpper;
			unitPriceUpper = exchange;
		}
		String releaseDateBefore;
		if(request.getParameter("release_date_before").equals("")){
			releaseDateBefore = "";
		}else {
			releaseDateBefore = request.getParameter("release_date_before");
		}
		String releaseDateAfter;
		if(request.getParameter("release_date_after").equals("")){
			releaseDateAfter = "";
		}else {
			releaseDateAfter = request.getParameter("release_date_after");
		}
		if(!(releaseDateBefore.equals("")) && !(releaseDateAfter.equals(""))) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date exchangeB = new Date();
			Date exchangeA = new Date();

			try {
				exchangeB = sdf.parse(releaseDateBefore);
			} catch (ParseException e) {
				releaseDateBefore = null;
			}
			try {
				exchangeA = sdf.parse(releaseDateAfter);
			} catch (ParseException e) {
				releaseDateAfter = null;
			}
			if(exchangeA.before(exchangeB)) {
				String exchange = releaseDateBefore;
				releaseDateBefore = releaseDateAfter ;
				releaseDateAfter = exchange;
			}
		}
		String makerName;
		if(request.getParameter("maker_name").equals("")){
			makerName = "";
		}else {
			makerName = request.getParameter("maker_name");
		}
		//変数をデータクラスへ格納
		DS = new DataSearch(productCd,productName,unitPriceLower,unitPriceUpper,releaseDateBefore,releaseDateAfter,makerName);
		return DS;
	}

	public DataProduct setDataProduct(HttpServletRequest request) throws DataFormatExpection{
		DataProduct DP;
		String productCd;
		String productName;
		int unitPrice;
		String releaseDate;
		String makerName;
		if(!request.getParameter("product_cd").equals("") && !request.getParameter("product_name").equals("") && !request.getParameter("unit_price").equals("")
				&& !request.getParameter("release_date").equals("") && !request.getParameter("maker_name").contentEquals("")) {
			productCd = request.getParameter("product_cd");
			productName = request.getParameter("product_name");
			unitPrice = Integer.parseInt(request.getParameter("unit_price"));
			releaseDate = request.getParameter("release_date");
			makerName = request.getParameter("maker_name");
			DP = new DataProduct(productCd,productName,unitPrice,releaseDate,makerName);
		}else{
			throw new DataFormatExpection ("入力項目は全て入力し、商品コードは重複させてはいけません。");
		}
		return DP;
	}

	private String parseDigit(int maxCd) {
		//フォーマット(00000)する。
		int digit = 5;
		//残桁数を算出
		int zeroCount = digit - String.valueOf(maxCd).length();
		String headZero ="";
		//残桁数だけ0を埋める
		for(int i = 0; i < zeroCount; i++) {
			headZero += "0";
		}
		return headZero + maxCd;

	}
	public String getNextCd() {
		int maxCd = Collections.max(DAL.getListCd()) + 1;
		//フォーマット(00000)して返す。
		return parseDigit(maxCd);
	}
	public void regist(DataProduct DP) throws DataFormatExpection{
		if(!(DAL.regist(DP))) {
			throw new DataFormatExpection("入力項目は全て入力し、商品コードは重複させてはいけません。");
		}
	}
	public void remove(String cd) {
		DAL.remove(cd);
	}
	public void update(DataProduct updateDP,String beforeCd) throws DataFormatExpection{
		DataProduct beforeDP = getCdResult(beforeCd);
		if(!(DAL.update(updateDP,beforeDP))) {
			throw new DataFormatExpection("入力項目は全て入力し、商品コードは重複させてはいけません。");
		}
	}
}
