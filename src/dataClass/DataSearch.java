package dataClass;

public class DataSearch {
	private String productCd;
	private String productName;
	private int unitPriceUpper;
	private int unitPriceLower;
	private String releaseDateBefore;
	private String releaseDateAfter;
	private String makerName;
	public DataSearch() {}
	public DataSearch(String productCd, String productName, int unitPriceLower,
			int unitPriceUpper, String releaseDateBefore, String releaseDateAfter, String makerName){
		this.productCd = productCd;
		this.productName = productName;
		this.unitPriceUpper = unitPriceUpper;
		this.unitPriceLower = unitPriceLower;
		this.releaseDateBefore = releaseDateBefore;
		this.releaseDateAfter = releaseDateAfter;
		this.makerName = makerName;
	}
	public String getProductCd() {
		return productCd;
	}
	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getUnitPriceUpper() {
		return unitPriceUpper;
	}
	public void setUnitPriceUpper(int unitPriceUpper) {
		this.unitPriceUpper = unitPriceUpper;
	}
	public int getUnitPriceLower() {
		return unitPriceLower;
	}
	public void setUnitPriceLower(int unitPriceLower) {
		this.unitPriceLower = unitPriceLower;
	}
	public String getReleaseDateBefore() {
		return releaseDateBefore;
	}
	public void setReleaseDateBefore(String releaseDateBefore) {
		this.releaseDateBefore = releaseDateBefore;
	}
	public String getReleaseDateAfter() {
		return releaseDateAfter;
	}
	public void setReleaseDateAfter(String releaseDateAfter) {
		this.releaseDateAfter = releaseDateAfter;
	}
	public String getMakerName() {
		return makerName;
	}
	public void setMakerName(String makerName) {
		this.makerName = makerName;
	}
}
