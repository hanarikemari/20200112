package dataClass;

public class DataProduct {
	private String productCd;
	private String productName;
	private int unitPrice;
	private String releaseDate;
	private String makerName;
	public DataProduct() {};
	public DataProduct(String productCd, String productName, int unitPrice,
			String releaseDate, String makerName){
		this.productCd = productCd;
		this.productName = productName;
		this.unitPrice = unitPrice;
		this.releaseDate = releaseDate;
		this.makerName = makerName;
	}
	public String getProductCd() {
		return productCd;
	}
	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(int unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getMakerName() {
		return makerName;
	}
	public void setMakerName(String makerName) {
		this.makerName = makerName;
	}

}
