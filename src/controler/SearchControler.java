package controler;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dataClass.DataProduct;
import dataClass.DataSearch;
import logics.DataFormatExpection;
import logics.ProductSearchLogic;

/**
 * Servlet implementation class SearchControler
 */
@WebServlet("/SearchControler")
public class SearchControler extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchControler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("SearchProduct.jsp");
		dispatcher.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		ProductSearchLogic PSL = new ProductSearchLogic();
		RequestDispatcher dispatcher = request.getRequestDispatcher("OperateProduct.jsp");
		switch(request.getParameter("name")) {
		case "search":
			DataSearch DS = PSL.setDataSearch(request);
			//検索結果
			request.setAttribute("result", PSL.getSearchResult(DS));
			//検索条件
			request.setAttribute("DataSearch", DS);
			doGet(request,response);
			return;
		case "new":
			request.setAttribute("nextCd", PSL.getNextCd());
			break;
		case "alt":
			String productCd = request.getParameter("product_cd");
			request.setAttribute("result", PSL.getCdResult(productCd));
			break;

		case "confirm":
			break;
		case "newDo":
			request.setAttribute("nextCd", PSL.getNextCd());
			try {
				DataProduct DP = PSL.setDataProduct(request);
				PSL.regist(DP);
			}catch(DataFormatExpection e) {
				request.setAttribute("error", e.getMessage());
			}
			break;
		case "altDo":
			String beforeCd =request.getParameter("product_cd_b");
			try {
				DataProduct updateDP = PSL.setDataProduct(request);
				PSL.update(updateDP,beforeCd);
				DataSearch altDS = new DataSearch();
				altDS.setProductCd(updateDP.getProductCd());
				request.setAttribute("result", PSL.getSearchResult(altDS));
				doGet(request,response);
				return;
			}catch(DataFormatExpection e) {
				request.setAttribute("result", PSL.getCdResult(beforeCd));
				request.setAttribute("error", e.getMessage());
			}
			break;
		case "removeDo":
			PSL.remove(request.getParameter("product_cd"));
			doGet(request,response);
			return;

		}
		dispatcher.forward(request,response);


	}

}
